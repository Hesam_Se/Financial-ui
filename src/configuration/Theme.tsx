import { createMuiTheme } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';
import amber from '@material-ui/core/colors/amber';
import red from '@material-ui/core/colors/red';

const Theme = createMuiTheme({
    palette: {
        primary: {
            light: grey[500],
            main: grey[900],
            dark: grey[900],
        },
        secondary: {
            light: amber[300],
            main: amber[500],
            dark: amber[700],
        },
        error: {
            light: red[300],
            main: red[500],
            dark: red[700],
        }
    },
    typography: {
        fontFamily: [
            'yekan',
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    direction: 'rtl',

    overrides: {
        MuiPickersDay: {
            fontFamily : 'yekan',
            day: {
                fontFamily: 'yekan'
            },
            selected: {
                fontFamily: 'yekan'
            },
            current: {
                fontFamily: 'yekan'
            },
        },
    } as any,
});

export default Theme;