import * as React from "react";
import {Route} from "react-router-dom";
import UsersListContainer from "../core/users/UsersListContainer";
import LoanListContainer from "../core/loans/LoanListContainer";
import ProfitsListContainer from "../core/Profit/ProfitListContainer";
import ReportContainer from "../core/reports/ReportContainer";

const Routes = () => {
    return (
        <div>
            <Route exact={true} path="/" component={ReportContainer}/>
            <Route path="/users" component={UsersListContainer}/>
            <Route path="/loans" component={LoanListContainer}/>
            <Route path="/profits" component={ProfitsListContainer}/>
        </div>
    );
};

export default Routes;