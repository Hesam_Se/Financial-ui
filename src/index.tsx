import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from "react-router-dom";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import Theme from "./configuration/Theme";

import { create } from 'jss';
import rtl from 'jss-rtl';
import JssProvider from 'react-jss/lib/JssProvider';
import { createGenerateClassName, jssPreset } from '@material-ui/core/styles';

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins,rtl() as any] });

// Custom Material-UI class name generator.
const generateClassName = createGenerateClassName();

ReactDOM.render(
<BrowserRouter>
            <MuiThemeProvider theme={Theme}>
                <JssProvider jss={jss} generateClassName={generateClassName}>
                    <App />
                </JssProvider>
            </MuiThemeProvider>
        </BrowserRouter>
    ,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
