export class Installment {
    public id : number;
    public loanId : number;
    public amount : number;
    public payDate : string;
    public transactionNumber?: number;
}