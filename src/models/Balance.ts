export class Balance {
    public id : number;
    public userId : number;
    public amount : number;
    public transactionNumber : string;
    public createDate : Date;
    public description : string;

    constructor(id,userId,amount,transactionNumber,createDate,description){
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.transactionNumber = transactionNumber;
        this.createDate = createDate;
        this.description = description;
    }
}