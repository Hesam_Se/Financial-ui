import LoanStatus from "./enums/LoanStatus";

export class Loan {
    public id : number;
    public userId : number;
    public username : string;
    public amount : number;
    public remain : number;
    public monthDuration: number;
    public firstInstallmentAmount: number;
    public installmentAmount: number;
    public wageFactor : 0.01 | 0.02;
    public wageAmount : number;
    public startDate : string;
    public status : LoanStatus;
    public description?: string;

    constructor(
        id : number,
        userId : number,
        username : string,
        amount : number,
        remain: number,
        monthDuration: number,
        firstInstallmentAmount: number,
        installmentAmount: number,
        wageFactor : 0.01 | 0.02,
        wageAmount : number,
        startDate : string,
        status : LoanStatus,
        description?: string
    ) {
        this.id = id;
        this.userId = userId;
        this.username = username;
        this.amount = amount;
        this.remain = remain;
        this.monthDuration = monthDuration;
        this.firstInstallmentAmount = firstInstallmentAmount;
        this.installmentAmount = installmentAmount;
        this.wageFactor = wageFactor;
        this.wageAmount = wageAmount;
        this.startDate = startDate;
        this.status = status;
        this.description = description;
    }
}