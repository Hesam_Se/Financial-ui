
export class User {
    public id : number;
    public firstName : string;
    public lastName : string;
    public nationalId : string;
    public loanId? : number;
    public phoneNumber : string;
    public balance : number;
    public createDate : string;

    constructor(id,firstName,lastName,nationalId,loanId,phoneNumber,balance,createDate){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nationalId = nationalId;
        this.loanId = loanId;
        this.phoneNumber = phoneNumber;
        this.balance = balance;
        this.createDate = createDate;
    }
}