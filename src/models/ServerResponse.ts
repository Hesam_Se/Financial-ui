
export class ServerResponse {
    public data : any;
    public title : string;
    public description : string;
    public code : string;
    public type : 'success' | 'error' | 'warning';

    constructor(data,messageTitle,messageDescription,messageCode,responseType) {
        this.data = data;
        this.title = messageTitle;
        this.description = messageDescription;
        this.code = messageCode;
        this.type = responseType;
    }
}