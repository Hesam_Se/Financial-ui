import * as React from 'react';
import './App.css';
import AppBar from "@material-ui/core/AppBar/AppBar";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Icon from "@material-ui/core/Icon/Icon";
import Typography from "@material-ui/core/es/Typography/Typography";
import Routes from "./configuration/Routes";
import MainMenu from "./core/mainMenu/MainMenu";

interface IStateTypes {
    isMenuOpen : boolean
}

class App extends React.Component<{},IStateTypes> {

    public state = {
        isMenuOpen : false,
    };

    public render() {
        return (
            <div className="App">
                <div id={'notification-container'}/>
                <AppBar position="static">
                    <Toolbar style={{height:65}} variant="dense">
                        <IconButton color="inherit" onClick={this.toggleMenu(true)}>
                            <Icon>menu</Icon>
                        </IconButton>
                        <Typography variant="title" color="inherit">
                            صندوق قرض الحسنه
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div id={'global-loading'}/>
                <MainMenu isOpen={this.state.isMenuOpen} toggleMenu={this.toggleMenu}/>
                <Routes/>
            </div>
        );
    };

    private toggleMenu = (isMenuOpen : boolean) => () => {
        this.setState({isMenuOpen});
    };

}

export default App;
