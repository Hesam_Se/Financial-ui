import DataService from "../common/services/DataService";

class ReportService {
    public static getBankAllFinance(){
        return DataService.GetJson('/report/bankAllFinance');
    }

    public static getCurrentFinance(){
        return DataService.GetJson('/report/currentFinance');
    }

    public static getAllRemaining(){
        return DataService.GetJson('/report/allRemaining');
    }

    public static getDebtors(){
        return DataService.GetJson('/report/debtors');
    }

    public static getCreditors(){
        return DataService.GetJson('/report/creditors');
    }
}

export default ReportService;