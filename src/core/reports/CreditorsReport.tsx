import * as React from 'react';
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableBody from "@material-ui/core/TableBody/TableBody";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import Creditor from "../../models/Creditor";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import Card from "@material-ui/core/Card/Card";
import {css} from "emotion";
import withTheme, {WithTheme} from "@material-ui/core/styles/withTheme";

interface IProps {
    creditors?: Creditor[];
}

class CreditorsReport extends React.Component<IProps & WithTheme,{}> {

    public render(){
        return(
            <Card className={this.getStyles()}>
                <Table>
                    <TableHead className={'th'}>
                        <TableRow>
                            <TableCell>#</TableCell>
                            <TableCell>نام</TableCell>
                            <TableCell>شماره وام</TableCell>
                            <TableCell>طلب</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.creditors ?
                            this.props.creditors.length === 0 ?
                                <TableRow>
                                    <TableCell colSpan={4}>موردی یافت نشد.</TableCell>
                                </TableRow>
                                :
                            this.props.creditors.map((creditor,index) =>
                                <TableRow key={index}>
                                    <TableCell>{index + 1}</TableCell>
                                    <TableCell>{creditor.username}</TableCell>
                                    <TableCell>{creditor.loanId}</TableCell>
                                    <TableCell>{creditor.credit}</TableCell>
                                </TableRow>
                            )
                            :
                            <CircularProgress size={100} color="secondary" />
                        }
                    </TableBody>
                </Table>
            </Card>
        )
    }

    private getStyles = () => {
        const {theme} = this.props;
        // language=LESS
        return css`
          & {
            display: inline-block;
            margin: 4%;
            max-height: 200px;
            overflow-y: auto;
            .th{
              background-color: ${theme.palette.secondary.light};
            }
          }
        `
    }
}

export default withTheme()(CreditorsReport);