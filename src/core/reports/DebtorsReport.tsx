import * as React from 'react';
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableBody from "@material-ui/core/TableBody/TableBody";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import Debtor from "../../models/Debtor";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import Card from "@material-ui/core/Card/Card";
import {css} from "emotion";
import withTheme, {WithTheme} from "@material-ui/core/styles/withTheme";

interface IProps {
    debtors?: Debtor[];
}

class DebtorsReport extends React.Component<IProps & WithTheme,{}> {

    public render(){
        return(
            <Card className={this.getStyles()}>
                <Table>
                    <TableHead className={'th'}>
                        <TableRow>
                            <TableCell>#</TableCell>
                            <TableCell>نام</TableCell>
                            <TableCell>شماره وام</TableCell>
                            <TableCell>بدهی</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.debtors ?
                            this.props.debtors.map((debtor,index) =>
                                <TableRow key={index}>
                                    <TableCell>{index + 1}</TableCell>
                                    <TableCell>{debtor.username}</TableCell>
                                    <TableCell>{debtor.loanId}</TableCell>
                                    <TableCell>{debtor.debt}</TableCell>
                                </TableRow>
                            )
                            :
                            <CircularProgress size={100} color="secondary" />
                        }
                    </TableBody>
                </Table>
            </Card>
        )
    }

    private getStyles = () => {
        const {theme} = this.props;
        // language=LESS
        return css`
          & {
            display: inline-block;
            margin: 4%;
            max-height: 200px;
            overflow-y: auto;
            .th{
              background-color: ${theme.palette.secondary.light};
            }
          }
        `
    }
}

export default withTheme()(DebtorsReport);