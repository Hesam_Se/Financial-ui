import * as React from 'react';
import Card from "@material-ui/core/Card/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import Typography from "@material-ui/core/Typography/Typography";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import {css} from "emotion";
import CardHeader from "@material-ui/core/CardHeader/CardHeader";
import withTheme, {WithTheme} from "@material-ui/core/styles/withTheme";

interface IProps {
    finance?: number;
    title : string;
}

class FinanceReport extends React.Component<IProps & WithTheme,{}>{

    public render(){
        const {finance} = this.props;
        return(
            <Card className={this.getStyles()}>
                <CardHeader className={'header'}/>
                <CardContent>
                    {finance ?
                        <div>
                            <Typography>
                                {this.props.title}
                            </Typography>
                            <Typography variant={"display1"}>
                                {finance}
                            </Typography>
                        </div>
                        :
                        <div>
                            <CircularProgress size={80} color="secondary" />
                        </div>
                    }

                </CardContent>
            </Card>
        );
    }

    private getStyles = () => {
        // language=LESS
        const {theme} = this.props;
        return css`
            & {
              width: 27%;
              margin: 3%;
              display: inline-block;
              border: 1px solid ${theme.palette.secondary.light};
              .header{
                background-color: ${theme.palette.secondary.light}
              }
            }
        `
    };
}

export default withTheme()(FinanceReport);