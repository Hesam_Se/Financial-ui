import * as React from 'react';
import ReportService from "./ReportService";
import FinanceReport from "./FinanceReport";
import DebtorsReport from "./DebtorsReport";
import Debtor from "../../models/Debtor";
import {css} from "emotion";
import CreditorsReport from "./CreditorsReport";
import Creditor from "../../models/Creditor";

interface IState {
    bankAllFinance?: number;
    currentFinance?: number;
    allRemaining?:number;
    debtors?: Debtor[];
    creditors?: Creditor[];
}

class ReportContainer extends React.Component<{},IState> {
    public state = {
        bankAllFinance:undefined,
        currentFinance:undefined,
        allRemaining:undefined,
        debtors:undefined,
        creditors:undefined,
    };

    public componentDidMount(){
        ReportService.getBankAllFinance()
            .then(finance => {
                this.setState({
                    bankAllFinance:finance
                })
            });

        ReportService.getCurrentFinance()
            .then(finance => {
                this.setState({
                    currentFinance:finance
                })
            });

        ReportService.getAllRemaining()
            .then(finance => {
                this.setState({allRemaining:finance});
            });

        ReportService.getDebtors()
            .then(debtors => {
                this.setState({debtors});
            });

        ReportService.getCreditors()
            .then(creditors => {
                this.setState({creditors});
            })
    }

    public render(){
        return(
            <div className={this.getStyles()}>
                <div>
                    <FinanceReport
                        finance={this.state.bankAllFinance}
                        title={'موجودی کل صندوق'}
                    />
                    <FinanceReport
                        finance={this.state.currentFinance}
                        title={'موجودی فعلی صندوق'}
                    />
                    <FinanceReport
                        finance={this.state.allRemaining}
                        title={'کل مطالبات'}
                    />
                </div>
                <div>
                    <DebtorsReport
                        debtors={this.state.debtors}
                    />
                    <CreditorsReport
                        creditors={this.state.creditors}
                    />
                </div>
            </div>
        );
    }

    private getStyles = () => {
        // language=LESS
        return css`
            & {
            }
        `
    };
}

export default ReportContainer;