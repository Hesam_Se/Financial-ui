import DataService from "../common/services/DataService";
import {Balance} from "../../models/Balance";

class BalanceService {

    public static getUserBalances(userId : number) : Promise<Balance[]> {
        return DataService.GetJson('/balance/user',userId.toString());
    }

    public static addBalance(balance : Balance) : Promise<number> {
        return DataService.PostJson('/balance',balance)
            .then(res => res ? res.balanceId : null);
    }

    public static editBalance(balance : Balance) {
        return DataService.PutJson('/balance',balance.id,balance);
    }

    public static deleteBalance(balanceId : number) {
        return DataService.DeleteJson('/balance',balanceId);
    }

}

export default BalanceService;