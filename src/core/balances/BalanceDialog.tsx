import * as React from 'react';
import {Balance} from "../../models/Balance";
import withTheme, {WithTheme} from "@material-ui/core/styles/withTheme";
import Dialog from "@material-ui/core/Dialog/Dialog";
import {css} from "emotion";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import Icon from "@material-ui/core/Icon/Icon";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Button from "@material-ui/core/Button/Button";
import DeleteBalanceDialog from "./DeleteBalanceDialog";
import CreateBalanceDialog from "./CreateBalanceDialog";

interface IPropTypes {
    userId:number;
    username: string;
    balances:Balance[];
    deleteBalance: (BalanceId : number) => void;
    createBalance: (balance: Balance) => Promise<any>;
    isOpen:boolean;
    onClose: () => void;
}

interface IStateTypes {
    currentBalance: Balance;
    isDeleteDialogOpen:boolean;
    isCreateDialogOpen:boolean;
}

class BalanceDialog extends React.Component<IPropTypes & WithTheme,IStateTypes> {
    public state = {
        currentBalance: undefined as any,
        isDeleteDialogOpen:false,
        isCreateDialogOpen: false,
    };

    public render() {
        return (
            <Dialog fullWidth={true} classes={{paper: styles(this.props)}} open={this.props.isOpen} onClose={this.props.onClose}>
                <DialogTitle>موجودی های {this.props.username}</DialogTitle>

                <DialogContent>
                    {this.content()}
                </DialogContent>
                <Button
                    classes={{root:"add-btn"}}
                    color={"secondary"}
                    variant={"fab"}
                    onClick={this.toggleDialog('isCreateDialogOpen',true,this.state.currentBalance)}
                >
                    <Icon>add</Icon>
                </Button>
                <DeleteBalanceDialog
                    isOpen={this.state.isDeleteDialogOpen}
                    onClose={this.toggleDialog('isDeleteDialogOpen',false,undefined as any)}
                    onSubmit={this.props.deleteBalance}
                    balance={this.state.currentBalance}
                    username={this.props.username}
                />
                <CreateBalanceDialog
                    userId={this.props.userId}
                    isOpen={this.state.isCreateDialogOpen}
                    onClose={this.toggleDialog('isCreateDialogOpen',false,undefined as any)}
                    onSubmit={this.props.createBalance}
                />
            </Dialog>
        );
    };

    private content = () => {
        const {balances} = this.props;
        return(
            <Paper className={'paper'}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>#</TableCell>
                            <TableCell>مبلغ</TableCell>
                            <TableCell>تاریخ پرداخت</TableCell>
                            <TableCell>شماره تراکنش</TableCell>
                            <TableCell>توضیحات</TableCell>
                            <TableCell>عملیات</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {balances.map((balance,index) => {
                            return (
                                <TableRow key={index}>
                                    <TableCell component="th" scope="row">
                                        {balance.id}
                                    </TableCell>
                                    <TableCell>{balance.amount}</TableCell>
                                    <TableCell>{balance.createDate}</TableCell>
                                    <TableCell>{balance.transactionNumber}</TableCell>
                                    <TableCell>{balance.description}</TableCell>
                                    <TableCell>
                                        <IconButton className={'edit-btn'} >
                                            <Icon>edit</Icon>
                                        </IconButton>
                                        <IconButton className={'delete-btn'} onClick={this.toggleDialog('isDeleteDialogOpen',true,balance)}>
                                            <Icon>delete</Icon>
                                        </IconButton>

                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        )
    };

    private toggleDialog = (dialogName : string,isOpen:boolean,currentBalance: Balance) => () => {
        this.setState({[dialogName] : isOpen,currentBalance} as any);
    }
}

const styles = (props) => {
    // language=LESS
    return css`
        & {
        position: absolute;
          max-width: 900px;
          .paper {
            margin-top: 5px;
          }
          .delete-btn{
            color: ${props.theme.palette.error.main};
          }
          .add-btn {
                position: absolute;
                right: 20px;
                bottom: 20px;
                z-index: 1;
                width: 75px;
                height: 75px;
            }
        }
    `
};

export default withTheme()(BalanceDialog);