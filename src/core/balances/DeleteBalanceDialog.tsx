import * as React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import {css} from "emotion";
import Typography from "@material-ui/core/Typography/Typography";
import {Balance} from "../../models/Balance";

interface IProps {
    balance? : Balance;
    username : string;
    isOpen : boolean;
    onClose : () => void;
    onSubmit : (balanceId: number) => void;
}


class DeleteBalanceDialog extends React.Component<IProps,{}> {
    constructor(props) {
        super(props);
    }

    public render() {
        const balance = this.props.balance as Balance;
        return (
            <Dialog classes={{root: styles(this.props)}} open={this.props.isOpen} onClose={this.props.onClose}>
                <DialogTitle>حذف قسط</DialogTitle>
                <DialogContent>
                    {balance &&
                    <Typography variant={'body2'}>
                        آیا مایل به حذف موجودی شماره {(this.props.balance as any).id} متعلق به {this.props.username} هستید؟
                    </Typography>
                    }
                </DialogContent>
                <DialogActions>
                    <Button variant={'raised'} color={'secondary'} onClick={this.onSubmit}>حذف</Button>
                    <Button variant={'raised'} onClick={this.props.onClose}>انصراف</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private onSubmit = () => {
        const balance = this.props.balance as Balance;
        this.props.onSubmit(balance.id);
        this.props.onClose();
    };
}

const styles = (props) => {
    return css`
        .row {
            display : flex
        }
        .col {
            flex : 50%;
        }
        .form-control {
            margin-left : 20px;
        }
    `
};

export default DeleteBalanceDialog;