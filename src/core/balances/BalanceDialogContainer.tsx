import * as React from 'react';
import {Balance} from "../../models/Balance";
import BalanceService from "./BalanceService";
import BalanceDialog from "./BalanceDialog";

interface IPropTypes {
    isOpen: boolean;
    onClose : () => void;
    userId : number;
    username : string;
}

interface IStateTypes {
    balances:Balance[];
}

class BalanceDialogContainer extends React.Component<IPropTypes,IStateTypes>{
    public state = {
        balances:[] as Balance[],
    };

    public componentDidUpdate(prevProps){
        if(!prevProps.isOpen && prevProps.isOpen !== this.props.isOpen){
            BalanceService.getUserBalances(this.props.userId)
                .then(balances => {
                    this.setState({balances});
                });
        }
    }

    public render(){
        return(
            <BalanceDialog
                balances={this.state.balances}
                createBalance={this.createBalance}
                deleteBalance={this.deleteBalance}
                isOpen={this.props.isOpen}
                userId={this.props.userId}
                username={this.props.username}
                onClose={this.onClose}
            />
        )
    }

    private onClose = () => {
        this.props.onClose();
        this.setState({balances:[]});
    };

    private deleteBalance = (balanceId : number) => {
        BalanceService.deleteBalance(balanceId)
            .then((isDeleted)=>{
                if(isDeleted) {
                    const newBalances = this.state.balances.filter(a=>a.id !== balanceId);
                    this.setState({
                        balances:newBalances
                    })
                }
            });
    };

    private createBalance = (balance: Balance) => {
        return BalanceService.addBalance(balance)
            .then((balanceId)=>{
                if(balanceId) {
                    balance.id = balanceId;
                    this.setState({balances:[...this.state.balances,balance]});
                }
            })
    }
}

export default BalanceDialogContainer;