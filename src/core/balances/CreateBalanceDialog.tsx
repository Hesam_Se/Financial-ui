import * as React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button/Button";
import {Balance} from "../../models/Balance";
import PersianDatePicker from "../common/PersianDatePicker";

interface IPropTypes {
    userId: number;
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (balance: Balance) => Promise<any>;
}

interface IStateTypes {
    amount?: number;
    createDate: string;
    transactionNumber: number;
    description : string;
}

class CreateBalanceDialog extends React.Component<IPropTypes,IStateTypes>{

    public constructor(props){
        super(props);
        this.state = this.initState();
    }

    public render(){
        return(
            <Dialog
                open={this.props.isOpen}
                onClose={this.props.onClose}
            >
                <DialogTitle>
                    ایجاد موجودی جدید
                </DialogTitle>

                <DialogContent>
                    <div>
                        <TextField
                            label={'مبلغ'}
                            value={this.state.amount ? this.state.amount : ""}
                            onChange={this.handleChange('amount')}
                        />
                    </div>

                    <div>
                        <PersianDatePicker
                            label={'تاریخ پرداخت'}
                            value={this.state.createDate}
                            onChange={this.handleChange('createDate')}
                        />
                    </div>

                    <div>
                        <TextField
                            label={'شماره تراکنش'}
                            value={this.state.transactionNumber > 0 ? this.state.transactionNumber : ""}
                            onChange={this.handleChange('transactionNumber')}
                        />
                    </div>
                    <div>
                        <TextField
                            label={'توضیحات'}
                            value={this.state.description}
                            onChange={this.handleChange('description')}
                        />
                    </div>
                </DialogContent>

                <DialogActions>
                    <Button variant={'raised'} color={'secondary'} onClick={this.onSubmit}>ایجاد</Button>
                    <Button variant={'raised'} onClick={this.props.onClose}>انصراف</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private initState = () : IStateTypes => {
        return {
            amount: 0,
            createDate:"",
            transactionNumber:0,
            description:"",
        }
    };

    private onSubmit = () => {
        const balance = new Balance(null,
            this.props.userId,
            this.state.amount,
            this.state.transactionNumber,
            this.state.createDate,
            this.state.description);


        this.props.onSubmit(balance).then(()=>{
            this.props.onClose();
            this.setState(this.initState());
        });
    };

    private handleChange = (fieldName: string) => (event) => {
        this.setState({[fieldName]: event.target.value} as any);
    };

}

export default CreateBalanceDialog;