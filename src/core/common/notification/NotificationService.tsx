import * as React from 'react';
import * as ReactDOM from "react-dom";
import Notification from "./Notification";


export const showNotificationSnackbar = (message,details,type) => {
    hideNotificationSnackbar();

    ReactDOM.render(
        <Notification
            message={message}
            details={details}
            type={type}
        />,
        document.getElementById('notification-container')
    );
};

const hideNotificationSnackbar = () => {
    ReactDOM.render(
        [],
        document.getElementById('notification-container')
    );
};