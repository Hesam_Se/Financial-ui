import * as React from 'react';
import classNames from 'classnames';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import {withStyles} from '@material-ui/core/styles';
import Icon from "@material-ui/core/Icon/Icon";
import IconButton from "@material-ui/core/IconButton/IconButton";

const variantIcon = {
    success: 'check-circle',
    warning: 'warning',
    error: 'error',
    info: 'info',
};

const styles1 = theme => ({
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.dark,
    },
    warning: {
        backgroundColor: amber[700],
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginLeft: theme.spacing.unit,
    },
    message: {
        display: 'flex',
        alignItems: 'center',
        fontFamily: 'yekan',
    }
});

function MySnackbarContent(props) {
    const { classes, className, message,details, onClose, variant, ...other } = props;
    const icon = variantIcon[variant];
    return (
        <SnackbarContent
            className={classNames(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={classNames(classes.icon, classes.iconVariant)}>{icon}</Icon>
                    {message}
                    <br/>
                    {details}
                </span>
                }
            action={variant === 'error' && [
                <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    className={classes.close}
                    onClick={onClose}
                >
                    <Icon className={classes.icon}>
                        close
                    </Icon>
                </IconButton>,
            ]}
            {...other}
        />
    );
}


const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);

const styles2 = theme => ({
    margin: {
        margin: theme.spacing.unit,
    },
});

interface IPropTypes {
    message: string;
    details: string;
    type: "success" | "error";
}

interface IStateTypes {
    open: boolean;
}

class CustomizedSnackbars extends React.Component<IPropTypes,IStateTypes> {
    public state = {
        open: true,
    };

    public render() {
        return (
            <div>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                    }}
                    open={this.state.open}
                    autoHideDuration={this.props.type === 'success' ? 1000 : 999999}
                    onClose={this.handleClose}
                >
                    <MySnackbarContentWrapper
                        onClose={this.handleClose}
                        variant={this.props.type}
                        message={this.props.message}
                        details={this.props.details}
                    />
                </Snackbar>
            </div>
        );
    }


    private handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ open: false });
    };
}

export default withStyles(styles2)(CustomizedSnackbars);