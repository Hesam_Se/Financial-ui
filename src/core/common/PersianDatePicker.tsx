import * as React from 'react';
import DatePicker from 'material-ui-pickers/DatePicker';
import {MuiPickersUtilsProvider,} from 'material-ui-pickers';
import jMoment from 'moment-jalaali';
import JalaliUtils from 'material-ui-pickers-jalali-utils';

jMoment.loadPersian({ dialect: 'persian-modern', usePersianDigits: false });

interface IProps {
    onChange: (dateString : Event) => void;
    label : string;
    value : string;
}

interface IState {
    selectedDate: any;
}

class PersianDatePicker extends React.Component<IProps,IState> {
    public state = {
        selectedDate: jMoment()
    };

    public componentDidMount(){
        this.updateOutput(this.state.selectedDate);
    }

    public render(){
        const textFieldProps = {
            label: this.props.label
        };
        return(
            <MuiPickersUtilsProvider utils={JalaliUtils} locale="fa">
                    <DatePicker
                        clearable={false}
                        autoOk={true}
                        okLabel="تأیید"
                        cancelLabel="لغو"
                        labelFunc={date => (date ? date.format('jYY/jMM/jDD') : '')}
                        value={this.state.selectedDate}
                        onChange={this.handleDateChange}
                        animateYearScrolling={false}
                        {...textFieldProps}
                    />
            </MuiPickersUtilsProvider>
        )
    }

    private handleDateChange = date => {
        this.setState({ selectedDate: date },()=>{this.updateOutput(this.state.selectedDate)});
    };

    private updateOutput = (date) => {
        const dateString = date ? date.format('jYY/jMM/jDD') : '';

        const ev = { // mock event
            target: {
                value:dateString
            }
        };
        this.props.onChange(ev as any);
    }
}

export default PersianDatePicker;