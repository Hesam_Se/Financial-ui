import * as React from 'react';
import LinearProgress from "@material-ui/core/LinearProgress/LinearProgress";

const Loading = () => {
    return (
        <LinearProgress  />
    );
};

export default Loading;