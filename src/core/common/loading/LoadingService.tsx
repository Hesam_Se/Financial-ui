import * as React from 'react';
import * as ReactDOM from "react-dom";
import Loading from "./Loading";

export const showLoading = () => {
    ReactDOM.render(
        <Loading/>,
        document.getElementById('global-loading')
    );
};

export const hideLoading = () => {
    ReactDOM.render(
        [],
        document.getElementById('global-loading')
    );
};