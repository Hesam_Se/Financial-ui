import {hideLoading, showLoading} from "../loading/LoadingService";
import {showNotificationSnackbar} from "../notification/NotificationService";


class DataService {
    public static GetJson(url: string,id = '',showNotification = false) : Promise<any> {
        showLoading();
        return fetch(this.prefix + url + '/' + id,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET",
            })
            .then(res => this.checkStatus(res))
            .then(res => res.json())
            .then(res => this.showSuccess(res,showNotification))
            .then(res => res.data)
            .catch(res => this.showError(res))
            .then(res => {hideLoading(); return res;})

    }

    public static PostJson(url : string,requestBody : any) : Promise<any> {
        showLoading();
        return fetch(this.prefix + url,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(requestBody)
            })
            .then(res => this.checkStatus(res))
            .then(res => res.json())
            .then(res => this.showSuccess(res,true))
            .then(res => res.data)
            .catch(res => this.showError(res))
            .then(res => {hideLoading(); return res;})

    }

    public static PutJson(url : string,id:string | number,requestBody : any) : Promise<any> {
        showLoading();
        return fetch(this.prefix + url + "/" + id,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body: JSON.stringify(requestBody)
            })
            .then(res => this.checkStatus(res))
            .then(res => res.json())
            .then(res => this.showSuccess(res,true))
            .then(res => res.data ? res.data.isEdited : null)
            .catch(res => this.showError(res))
            .then(res => {hideLoading(); return res;})
    }

    public static DeleteJson(url : string,id: string | number) : Promise<any> {
        showLoading();
        return fetch(this.prefix + url + "/" + id,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "DELETE"
        })
            .then(res => this.checkStatus(res))
            .then(res => res.json())
            .then(res => this.showSuccess(res,true))
            .then(res => res.data ? res.data.isDeleted : undefined)
            .catch(res => this.showError(res))
            .then(res => {hideLoading(); return res;})
    };

    private static prefix = 'api';

    private static checkStatus = (res) => {
        if(res.status !== 200){
            throw res;
        }
        return res;
    };

    private static showSuccess = (res,showNotification) => {
        if(showNotification){
            showNotificationSnackbar(res.description,null,"success");
        }
        return res;
    };

    private static showError = (res) => {
        res.json().then((json) => {
            showNotificationSnackbar(json.description,JSON.stringify(json.details),"error");
        });
    };
}

export default DataService;