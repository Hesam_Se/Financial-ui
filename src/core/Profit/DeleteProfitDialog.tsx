import * as React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import Profit from "../../models/Profit";
import {css} from "emotion";
import Typography from "@material-ui/core/Typography/Typography";

interface IProps {
    profit? : Profit;
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (profit: Profit) => void;
}


class DeleteProfitDialog extends React.Component<IProps,{}> {
    constructor(props) {
        super(props);
    }

    public render() {
        const profit = this.props.profit as Profit;
        return (
            <Dialog classes={{root: styles(this.props)}} open={this.props.isOpen} onClose={this.props.onClose}>
                <DialogTitle>حذف سود</DialogTitle>
                <DialogContent>
                    {profit &&
                    <Typography variant={'body2'}>
                        آیا مایل به حذف سود به مبلغ {profit.amount} در تاریخ {profit.payDate} هستید؟
                    </Typography>
                    }
                </DialogContent>
                <DialogActions>
                    <Button variant={'raised'} color={'secondary'} onClick={this.onSubmit}>حذف</Button>
                    <Button variant={'raised'} onClick={this.props.onClose}>انصراف</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private onSubmit = () => {
        const profit = this.props.profit as Profit;
        this.props.onSubmit(profit);
        this.props.onClose();
    };
}

const styles = (props) => {
    return css`
        .row {
            display : flex
        }
        .col {
            flex : 50%;
        }
        .form-control {
            margin-left : 20px;
        }
    `
};

export default DeleteProfitDialog;