import Profit from "../../models/Profit";
import DataService from "../common/services/DataService";

class ProfitService {
    public static getAllProfits() : Promise<Profit[]> {
        return DataService.GetJson("/profit");
    }

    public static AddProfit(profit : Profit) : Promise<any>{
        return DataService.PostJson("/profit",profit).then(res => res ? res.profitId : null);
    }

    public static DeleteProfit(profitId : number) : Promise<any> {
        return DataService.DeleteJson("/profit",profitId)
    }
}

export default ProfitService;