import * as React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import Profit from "../../models/Profit";
import {css} from "emotion";
import ProfitType from "../../models/enums/ProfitType";
import PersianDatePicker from "../common/PersianDatePicker";

interface IState {
    amount: number;
    payDate: string;
    type: ProfitType;
    description : string;
}

interface IProps {
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (profit: Profit, initBalance?: number) => void;
}


class AddProfitDialog extends React.Component<IProps, IState> {
    constructor(props) {
        super(props);
        this.state = {
            amount:0,
            payDate:"",
            type:ProfitType.BankProfit, // only adds bank profit
            description:""
        }
    }

    public render() {
        return (
            <Dialog classes={{root: styles(this.props)}} open={this.props.isOpen} onClose={this.props.onClose}>
                <DialogTitle>ثبت سود جدید</DialogTitle>
                <DialogContent>
                    <div className={'row'}>
                        <TextField
                            label={"مبلغ"}
                            value={this.state.amount}
                            onChange={this.handleChange('amount')}
                            className={'form-control col'}
                        />
                        <div className={'form-control col'}>
                            <PersianDatePicker
                                label={"تاریخ واریز"}
                                value={this.state.payDate}
                                onChange={this.handleChange('payDate')}
                            />
                        </div>

                    </div>

                    <div className={'row'}>
                        <TextField
                            label={"توضیحات"}
                            value={this.state.description}
                            onChange={this.handleChange('description')}
                            className={'form-control col'}
                        />
                    </div>

                </DialogContent>
                <DialogActions>
                    <Button variant={'raised'} color={'secondary'} onClick={this.onSubmit}>ایجاد</Button>
                    <Button variant={'raised'} onClick={this.props.onClose}>انصراف</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private initState = () => {
        this.setState({
            amount:0,
            payDate:"",
            description:"",
            type: ProfitType.BankProfit
        });
    };

    private handleChange = (fieldName: string) => (event) => {
        this.setState({[fieldName]: event.target.value} as any);
    };

    private onSubmit = () => {
        const profit = new Profit(0,this.state.amount,this.state.payDate,this.state.type,this.state.description);
        this.props.onSubmit(profit);
        this.props.onClose();
        this.initState();
    };
}

const styles = (props) => {
    return css`
        .row {
            display : flex
        }
        .col {
            flex : 50%;
        }
        .form-control {
            margin-left : 20px;
        }
    `
};

export default AddProfitDialog;