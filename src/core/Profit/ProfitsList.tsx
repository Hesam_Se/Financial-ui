import * as React from 'react';
import Table from "@material-ui/core/Table/Table";
import Paper from "@material-ui/core/es/Paper/Paper";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import {WithTheme} from "@material-ui/core";
import {css} from "emotion";
import Profit from "../../models/Profit";
import withTheme from "@material-ui/core/styles/withTheme";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Icon from "@material-ui/core/es/Icon/Icon";
import Button from "@material-ui/core/Button/Button";
import AddProfitDialog from "./AddProfitDialog";
import DeleteProfitDialog from "./DeleteProfitDialog";
import TableFooter from "@material-ui/core/TableFooter/TableFooter";
import ProfitType from "../../models/enums/ProfitType";


interface IProps {
    profits : Profit[];
    addProfit : (profit : Profit,initBalance : number) => void;
    deleteProfit : (profit : Profit) => void;
}

interface IState {
    isAddDialogOpen : boolean;
    isDeleteDialogOpen : boolean;
    clickedProfit? : Profit;
}

class ProfitsList extends React.Component<IProps & WithTheme,IState> {
    constructor(props){
        super(props);
        this.state = {
            isAddDialogOpen : false,
            isDeleteDialogOpen : false,
            clickedProfit : undefined,
        }
    }

    public render() {
        const {profits} = this.props;
        let sumAll = 0;
        return(
            <div className={style(this.props)}>
                <Paper className={'paper'}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>#</TableCell>
                                <TableCell>نوع</TableCell>
                                <TableCell>مقدار</TableCell>
                                <TableCell>تاریخ واریز</TableCell>
                                <TableCell>توضیحات</TableCell>
                                <TableCell>عملیات</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {profits.map((profit,index) => {
                                sumAll += parseInt(profit.amount as any,10);
                                return (
                                    <TableRow key={index}>
                                        <TableCell component="th" scope="row">
                                            {index + 1}
                                        </TableCell>
                                        <TableCell>{this.getTypeName(profit.type)}</TableCell>
                                        <TableCell>{profit.amount}</TableCell>
                                        <TableCell>{profit.payDate}</TableCell>
                                        <TableCell>{profit.description}</TableCell>
                                        <TableCell>
                                            <IconButton
                                                className={'delete-btn'}
                                                onClick={this.toggleDialog('isDeleteDialogOpen',true,profit)}
                                            >
                                                <Icon>delete</Icon>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TableCell>جمع کل</TableCell>
                                <TableCell/>
                                <TableCell colSpan={4} >{sumAll}</TableCell>
                            </TableRow>
                        </TableFooter>
                    </Table>
                </Paper>
                <Button
                    classes={{root:"add-btn"}}
                    color={"secondary"}
                    variant={"fab"}
                    onClick={this.toggleDialog('isAddDialogOpen',true)}
                >
                    <Icon>add</Icon>
                </Button>
                <AddProfitDialog
                    isOpen={this.state.isAddDialogOpen}
                    onClose={this.toggleDialog('isAddDialogOpen',false)}
                    onSubmit={this.props.addProfit}
                />
                <DeleteProfitDialog
                    profit={this.state.clickedProfit}
                    isOpen={this.state.isDeleteDialogOpen}
                    onClose={this.toggleDialog('isDeleteDialogOpen',false)}
                    onSubmit={this.props.deleteProfit}
                />
            </div>
        )
    }

    private toggleDialog = (dialogName,isOpen,clickedProfit?: Profit) => () => {
        this.setState({[dialogName] : isOpen,clickedProfit,isMenuOpen: false} as any);
    };

    private getTypeName = (type : ProfitType) : string => {
        if(type === ProfitType.BankProfit) {
            return "سود بانکی";
        }
        else if(type === ProfitType.LoanWage) {
            return "کارمزد وام";
        }
        else {
            return "";
        }

    }

}

const style = (props) => {
    const {theme} = props;
    return css`
        &{
            .paper {
                margin-right: 50px;
                margin-left: 50px;
                margin-top: 30px;
            
                .finance-btn {
                    color : ${theme.palette.secondary.main}
                }   
                .edit-btn {
                }
                .delete-btn {
                    color : ${theme.palette.error.main}
                }
            }
            .add-btn {
                position: fixed;
                right: 20px;
                bottom: 20px;
                z-index: 1;
                width: 75px;
                height: 75px;
            }
        }
    `;
};

export default withTheme()(ProfitsList);