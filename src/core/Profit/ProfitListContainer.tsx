import * as React from 'react';
import Profit from "../../models/Profit";
import ProfitsList from "./ProfitsList";
import ProfitService from "./ProfitService";

interface IState {
    profits : Profit[];
}

class ProfitsListContainer extends React.Component<{},IState>{
    constructor(props){
        super(props);
        this.state = {
            profits : [],
        }
    }

    public componentDidMount(){
        // todo : fetchProfit
        ProfitService.getAllProfits().then(profits => {
            this.setState({profits});
        });
    }

    public render(){
        return(
            <ProfitsList
                profits={this.state.profits}
                addProfit={this.addProfit}
                deleteProfit={this.deleteProfit}
            />
        );
    }

    private addProfit = (profit : Profit) => {
        ProfitService.AddProfit(profit).then((profitId)=>{
            if(profitId) {
                profit.id = profitId;
                this.setState({profits:[...this.state.profits,profit]});
            }
        });
    };

    private deleteProfit = (profit : Profit) => {
        ProfitService.DeleteProfit(profit.id).then((isDeleted)=>{
            if(isDeleted) {
                const {profits} = this.state;
                const updatedProfits = profits.filter(u => u.id !== profit.id);
                this.setState({profits:updatedProfits});
            }
        });
    };
}

export default ProfitsListContainer;