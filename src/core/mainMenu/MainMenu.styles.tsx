import {css} from "emotion";


const styles = (theme) => {

    return css`
        width: 250px;
        .items-container {
            width: 100%;
        }
        a {
            text-decoration:none;
        }
    `
};

export default styles;