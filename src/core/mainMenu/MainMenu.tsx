import * as React from 'react';
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer/SwipeableDrawer";
import MainMenuItems from "./MainMenuItems";
import styles from "./MainMenu.styles";
import {WithTheme} from "@material-ui/core";
import withTheme from "@material-ui/core/styles/withTheme";

interface IPropTypes {
    isOpen: boolean;
    toggleMenu : (open : boolean) => () => void;
};

const MainMenu = ({isOpen,toggleMenu,theme} : IPropTypes & WithTheme) => {
    return(
        <SwipeableDrawer
            anchor="left"
            open={isOpen}
            onClose={toggleMenu(false)}
            onOpen={toggleMenu(true)}
            onClick={toggleMenu(false)}
            classes={{paper:styles(theme)}}
        >
            <MainMenuItems/>
        </SwipeableDrawer>
    );
};

export default withTheme()(MainMenu);