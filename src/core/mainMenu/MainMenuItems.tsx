import * as React from 'react';
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import ListItemIcon from "@material-ui/core/ListItemIcon/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import Icon from "@material-ui/core/Icon/Icon";
import Paper from "@material-ui/core/Paper/Paper";
import Divider from "@material-ui/core/Divider/Divider";
import {Link} from "react-router-dom";

const MainMenuItems = () => {
    return(
            <Paper style={{width:'100%'}}>
                <Link to={'/'}>
                    <MenuItem>
                        <ListItemIcon>
                            <Icon>home</Icon>
                        </ListItemIcon>
                        <ListItemText inset={true} primary="خانه" />
                    </MenuItem>
                </Link>
                <Divider/>
                <Link to={'/users'}>
                    <MenuItem >
                        <ListItemIcon>
                            <Icon>account_circle</Icon>
                        </ListItemIcon>
                        <ListItemText inset={true} primary="کاربران" />
                    </MenuItem>
                </Link>
                <Divider/>
                <Link to={'/loans'}>
                    <MenuItem>
                        <ListItemIcon>
                            <Icon>folder</Icon>
                        </ListItemIcon>
                        <ListItemText inset={true} primary="وام ها" />
                    </MenuItem>
                </Link>
                <Divider/>
                <Link to={'/profits'}>
                    <MenuItem >
                        <ListItemIcon>
                            <Icon>attach_money</Icon>
                        </ListItemIcon>
                        <ListItemText inset={true} primary="سودها" />
                    </MenuItem>
                </Link>
            </Paper>
    );
};

export default MainMenuItems;