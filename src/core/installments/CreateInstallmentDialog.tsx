import * as React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button/Button";
import {Installment} from "../../models/Installment";
import PersianDatePicker from "../common/PersianDatePicker";

interface IPropTypes {
    loanId: number;
    defaultAmount: number;
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (installment: Installment) => Promise<any>;
}

interface IStateTypes {
    amount?: number;
    payDate: string;
    transactionNumber: number;
}

class CreateInstallmentDialog extends React.Component<IPropTypes,IStateTypes>{

    public constructor(props){
        super(props);
        this.state = this.initState(props);
    }

    public componentWillReceiveProps(newProps) {
        this.setState(this.initState(newProps));
    }

    public render(){
        return(
            <Dialog
                open={this.props.isOpen}
                onClose={this.props.onClose}
            >
                <DialogTitle>
                    ایجاد قسط جدید
                </DialogTitle>

                <DialogContent>
                    <div>
                        <TextField
                            label={'مبلغ'}
                            value={this.state.amount ? this.state.amount : ""}
                            onChange={this.handleChange('amount')}
                        />
                    </div>

                    <div>
                        <PersianDatePicker
                            label={'تاریخ پرداخت'}
                            value={this.state.payDate}
                            onChange={this.handleChange('payDate')}
                        />
                    </div>

                    <div>
                        <TextField
                            label={'شماره تراکنش'}
                            value={this.state.transactionNumber > 0 ? this.state.transactionNumber : ""}
                            onChange={this.handleChange('transactionNumber')}
                        />
                    </div>
                </DialogContent>

                <DialogActions>
                    <Button variant={'raised'} color={'secondary'} onClick={this.onSubmit}>ایجاد</Button>
                    <Button variant={'raised'} onClick={this.props.onClose}>انصراف</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private initState = (props) : IStateTypes => {
        return {
            amount: props.defaultAmount,
            payDate:"",
            transactionNumber:0,
        }
    };

    private onSubmit = () => {
        const installment = new Installment();
        installment.loanId = this.props.loanId;
        installment.amount = this.state.amount as any;
        installment.transactionNumber = this.state.transactionNumber;
        installment.payDate = this.state.payDate;

        this.props.onSubmit(installment).then(()=>{
            this.props.onClose();
            this.setState(this.initState(this.props));
        });
    };

    private handleChange = (fieldName: string) => (event) => {
        this.setState({[fieldName]: event.target.value} as any);
    };

}

export default CreateInstallmentDialog;