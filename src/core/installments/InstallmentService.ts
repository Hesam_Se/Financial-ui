import {Installment} from "../../models/Installment";
import DataService from "../common/services/DataService";

class InstallmentService {
    public static getInstallmentListByLoanId(loanId : number) : Promise<Installment[]> {
        return DataService.GetJson('/installment/loan',loanId.toString());
    }

    public static createInstallment(installment : Installment) : Promise<any> {
        return DataService.PostJson('/installment',installment)
            .then(res => res ? res.installmentId : null);
    }

    public static editInstallment(installment : Installment) : Promise<any> {
        return DataService.PutJson('/installment',installment.id,installment);

    }

    public static deleteInstallment(installmentId : number) : Promise<any> {
        return DataService.DeleteJson('/installment',installmentId);
    }
}

export default InstallmentService;