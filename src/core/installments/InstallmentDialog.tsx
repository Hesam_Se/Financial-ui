import * as React from 'react';
import {Installment} from "../../models/Installment";
import withTheme, {WithTheme} from "@material-ui/core/styles/withTheme";
import Dialog from "@material-ui/core/Dialog/Dialog";
import {css} from "emotion";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import Icon from "@material-ui/core/Icon/Icon";
import IconButton from "@material-ui/core/IconButton/IconButton";
import DeleteInstallmentDialog from "./DeleteInstallmentDialog";
import CreateInstallmentDialog from "./CreateInstallmentDialog";
import Button from "@material-ui/core/Button/Button";
import {Loan} from "../../models/Loan";

interface IPropTypes {
    loan: Loan;
    installments:Installment[];
    deleteInstallment: (installment: Installment) => void;
    createInstallment: (installment: Installment) => Promise<any>;
    isOpen:boolean;
    onClose: () => void;
}

interface IStateTypes {
    currentInstallment: Installment;
    isDeleteDialogOpen:boolean;
    isCreateDialogOpen:boolean;
}

class InstallmentDialog extends React.Component<IPropTypes & WithTheme,IStateTypes> {
    public state = {
        currentInstallment: undefined as any,
        isDeleteDialogOpen:false,
        isCreateDialogOpen: false,
    };

    public render() {
        return (
            <Dialog fullWidth={true} classes={{paper: styles(this.props)}} open={this.props.isOpen} onClose={this.props.onClose}>
                <DialogTitle>اقساط وام به شماره {this.props.loan ? this.props.loan.id : 0} متعلق به {this.props.loan ?this.props.loan.username : ""}</DialogTitle>

                <DialogContent>
                    {this.content()}
                </DialogContent>
                <Button
                    classes={{root:"add-btn"}}
                    color={"secondary"}
                    variant={"fab"}
                    onClick={this.toggleDialog('isCreateDialogOpen',true,this.state.currentInstallment)}
                >
                    <Icon>add</Icon>
                </Button>
                <DeleteInstallmentDialog
                    isOpen={this.state.isDeleteDialogOpen}
                    onClose={this.toggleDialog('isDeleteDialogOpen',false,undefined as any)}
                    onSubmit={this.props.deleteInstallment}
                    installment={this.state.currentInstallment}
                />
                <CreateInstallmentDialog
                    loanId={this.props.loan ? this.props.loan.id : 0}
                    defaultAmount={this.calculateDefaultAmount()}
                    isOpen={this.state.isCreateDialogOpen}
                    onClose={this.toggleDialog('isCreateDialogOpen',false,undefined as any)}
                    onSubmit={this.props.createInstallment}
                />
            </Dialog>
        );
    };

    private content = () => {
        const {installments} = this.props;
        return(
            <Paper className={'paper'}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>#</TableCell>
                            <TableCell>مبلغ</TableCell>
                            <TableCell>تاریخ پرداخت</TableCell>
                            <TableCell>شماره تراکنش</TableCell>
                            <TableCell>عملیات</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {installments.map((installment,index) => {
                            return (
                                <TableRow key={index}>
                                    <TableCell component="th" scope="row">
                                        {installment.id}
                                    </TableCell>
                                    <TableCell>{installment.amount}</TableCell>
                                    <TableCell>{installment.payDate}</TableCell>
                                    <TableCell>{installment.transactionNumber}</TableCell>
                                    <TableCell>
                                        <IconButton className={'edit-btn'} >
                                            <Icon>edit</Icon>
                                        </IconButton>
                                        <IconButton className={'delete-btn'} onClick={this.toggleDialog('isDeleteDialogOpen',true,installment)}>
                                            <Icon>delete</Icon>
                                        </IconButton>

                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        )
    };

    private toggleDialog = (dialogName : string,isOpen:boolean,currentInstallment: Installment) => () => {
        this.setState({[dialogName] : isOpen,currentInstallment} as any);
    }

    private calculateDefaultAmount = () => {
        let amount = 0;
        if(this.props.loan) {
            if(this.props.installments.length > 0){
                amount = this.props.loan.installmentAmount;
            }
            else {
                amount = this.props.loan.firstInstallmentAmount;
            }
        }

        return amount;
    }
}

const styles = (props) => {
    // language=LESS
    return css`
        & {
        position: absolute;
          max-width: 900px;
          .paper {
            margin-top: 5px;
          }
          .delete-btn{
            color: ${props.theme.palette.error.main};
          }
          .add-btn {
                position: absolute;
                right: 20px;
                bottom: 20px;
                z-index: 1;
                width: 75px;
                height: 75px;
            }
        }
    `
};

export default withTheme()(InstallmentDialog);