import * as React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import {css} from "emotion";
import Typography from "@material-ui/core/Typography/Typography";
import {Installment} from "../../models/Installment";

interface IProps {
    installment? : Installment;
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (installment : Installment) => void;
}


class DeleteInstallmentDialog extends React.Component<IProps,{}> {
    constructor(props) {
        super(props);
    }

    public render() {
        const installment = this.props.installment as Installment;
        return (
            <Dialog classes={{root: styles(this.props)}} open={this.props.isOpen} onClose={this.props.onClose}>
                <DialogTitle>حذف قسط</DialogTitle>
                <DialogContent>
                    {installment &&
                    <Typography variant={'body2'}>
                        آیا مایل به حذف قسط شماره {installment.id} متعلق به وام شماره {installment.loanId} هستید؟
                    </Typography>
                    }
                </DialogContent>
                <DialogActions>
                    <Button variant={'raised'} color={'secondary'} onClick={this.onSubmit}>حذف</Button>
                    <Button variant={'raised'} onClick={this.props.onClose}>انصراف</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private onSubmit = () => {
        const installment = this.props.installment as Installment;
        this.props.onSubmit(installment);
        this.props.onClose();
    };
}

const styles = (props) => {
    return css`
        .row {
            display : flex
        }
        .col {
            flex : 50%;
        }
        .form-control {
            margin-left : 20px;
        }
    `
};

export default DeleteInstallmentDialog;