import * as React from 'react';
import InstallmentDialog from "./InstallmentDialog";
import {Installment} from "../../models/Installment";
import InstallmentService from "./InstallmentService";
import {Loan} from "../../models/Loan";

interface IPropTypes {
    isOpen: boolean;
    onClose : () => void;
    loan: Loan;
    onRemainUpdate : (loanId : number, addedOrSubtractedAmount : number) => void;
}

interface IStateTypes {
    installments:Installment[];
}

class InstallmentDialogContainer extends React.Component<IPropTypes,IStateTypes>{
    public state = {
        installments:[] as Installment[],
    };
    public componentDidUpdate(prevProps){
        if(!prevProps.isOpen && prevProps.isOpen !== this.props.isOpen){
            InstallmentService.getInstallmentListByLoanId(this.props.loan.id)
                .then(installments => {
                    this.setState({installments});
                });
        }
    }

    public render(){
        return(
            <InstallmentDialog
                onClose={this.onClose}
                loan={this.props.loan}
                installments={this.state.installments}
                isOpen={this.props.isOpen}
                deleteInstallment={this.deleteInstallment}
                createInstallment={this.createInstallment}
            />
        )
    }

    private onClose = () => {
        this.props.onClose();
        this.setState({installments:[]});
    };

    private deleteInstallment = (installment : Installment) => {

        InstallmentService.deleteInstallment(installment.id)
            .then((isDeleted)=>{
                if(isDeleted) {
                    const newInstallments = this.state.installments.filter(a=>a.id !== installment.id);
                    this.setState({
                        installments:newInstallments
                    });
                    this.props.onRemainUpdate(installment.loanId,+installment.amount);
                }
            });
    };

    private createInstallment = (installment: Installment) => {
        return InstallmentService.createInstallment(installment)
            .then((installmentId)=>{
                if(installmentId) {
                    installment.id = installmentId;
                    this.setState({installments:[...this.state.installments,installment]});

                    this.props.onRemainUpdate(installment.loanId,-installment.amount);
                }
            })
    }
}

export default InstallmentDialogContainer;