import * as React from 'react';
import {Loan} from "../../models/Loan";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import {css} from "emotion";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import LoanStatus from "../../models/enums/LoanStatus";
import PersianDatePicker from "../common/PersianDatePicker";


interface IStateTypes {
    amount: number;
    wageFactor: 0.01 | 0.02;
    startDate: string;
}

interface IPropTypes {
    userId: number;
    username: string;
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (loan : Loan) => Promise<any>;
}

class CreateLoanDialog extends React.Component<IPropTypes,IStateTypes> {
    public state = {
        amount:0,
        wageFactor: 0.01 as 0.01 | 0.02,
        startDate: '',
        monthDuration:12,
        description: "",
    };

    public render(){
        return(
            <Dialog
                open={this.props.isOpen}
                onClose={this.props.onClose}
                classes={{paper:styles(this.props)}}
            >
                <DialogTitle>
                    ثبت وام برای {this.props.username}
                </DialogTitle>
                <DialogContent>
                    <div className={'row'}>
                        <TextField
                            value={this.state.amount}
                            className={'form-control'}
                            label={'مبلغ'}
                            onChange={this.handleChange('amount')}
                        />
                    </div>
                    <div className={'row'}>
                        <TextField
                            select={true}
                            label="کارمزد"
                            value={this.state.wageFactor}
                            onChange={this.handleChange('wageFactor')}
                            className={'form-control'}
                        >
                            <MenuItem key={0} value={0.01}>
                                1%
                            </MenuItem>
                            <MenuItem key={0} value={0.02}>
                                2%
                            </MenuItem>
                        </TextField>
                    </div>
                    <div className={'row'}>
                        <div className={'form-control'}>
                            <PersianDatePicker
                                value={this.state.startDate}
                                label={'تاریخ شروع'}
                                onChange={this.handleChange('startDate')}
                            />
                        </div>
                    </div>
                    <div className={'row'}>
                        <TextField
                            value={this.state.monthDuration}
                            label={'تعداد اقساط'}
                            onChange={this.handleChange('monthDuration')}
                            className={'form-control'}
                        />
                    </div>
                    <div className={'row'}>
                        <TextField
                            value={this.state.description}
                            label={'توضیحات'}
                            onChange={this.handleChange('description')}
                            className={'form-control'}
                        />
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button variant={'raised'} color={'secondary'} onClick={this.onSubmit}>ایجاد</Button>
                    <Button variant={'raised'} onClick={this.props.onClose}>انصراف</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private handleChange = (stateName) => (ev) => {
        this.setState({[stateName] : ev.target.value} as any);
    };

    private onSubmit = () => {
        const loan = new Loan(
            0,
            this.props.userId,
            this.props.username,
            this.state.amount,
            this.state.amount, // remain
            this.state.monthDuration,
            0,
            0,
            this.state.wageFactor,
            0,
            this.state.startDate,
            LoanStatus.open,
            this.state.description
        );
        this.props.onSubmit(loan).then(()=> {
            this.props.onClose();
            this.setState({
                amount: 0,
                wageFactor: 0.01 as 0.01 | 0.02,
                startDate: ''
            });
        });
    };

}
const styles = (props) => {
    return css`
        .row {
            display : flex
        }
        .col {
            flex : 50%;
        }
        .form-control {
            margin-left : 20px;
        }
    `
};

export default CreateLoanDialog;