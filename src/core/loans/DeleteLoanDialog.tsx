import * as React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import {css} from "emotion";
import Typography from "@material-ui/core/Typography/Typography";
import {Loan} from "../../models/Loan";

interface IProps {
    loan? : Loan;
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (loanId: number) => void;
}


class DeleteLoanDialog extends React.Component<IProps,{}> {
    constructor(props) {
        super(props);
    }

    public render() {
        const loan = this.props.loan as Loan;
        return (
            <Dialog classes={{root: styles(this.props)}} open={this.props.isOpen} onClose={this.props.onClose}>
                <DialogTitle>حذف وام</DialogTitle>
                <DialogContent>
                    {loan &&
                    <Typography variant={'body2'}>
                        آیا مایل به حذف وام شماره {loan.id} متعلق به {loan.username} هستید؟
                    </Typography>
                    }
                </DialogContent>
                <DialogActions>
                    <Button variant={'raised'} color={'secondary'} onClick={this.onSubmit}>حذف</Button>
                    <Button variant={'raised'} onClick={this.props.onClose}>انصراف</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private onSubmit = () => {
        const loan = this.props.loan as Loan;
        this.props.onSubmit(loan.id);
        this.props.onClose();
    };
}

const styles = (props) => {
    return css`
        .row {
            display : flex
        }
        .col {
            flex : 50%;
        }
        .form-control {
            margin-left : 20px;
        }
    `
};

export default DeleteLoanDialog;