import * as React from 'react';
import {Loan} from "../../models/Loan";
import LoanList from "./LoanList";
import {Installment} from "../../models/Installment";
import LoanService from "./LoanService";


interface IStateTypes {
    loans: Loan[];
}

class LoanListContainer extends React.Component<{},IStateTypes>{
    public state = {
        loans:[] as Loan[],
    };

    public componentDidMount(){
        LoanService.getAllLoans().then(loans =>{
            this.setState({loans});
        })
    }

    public render(){
        return(
            <LoanList
                loans={this.state.loans}
                editLoan={this.editLoan}
                deleteLoan={this.deleteLoan}
                getInstallmentList={this.getInstallmentList}
                addInstallment={this.addInstallment}
                editInstallment={this.editInstallment}
                deleteInstallment={this.deleteInstallment}
                updateRemain={this.updateRemain}
            />
        )
    }

    private editLoan = (loan : Loan) => {
        //
    };

    private deleteLoan = (loanId : number) => {
        LoanService.deleteLoan(loanId)
            .then((isDeleted)=>{
                if(isDeleted) {
                    const newLoans = this.state.loans.filter(l => l.id !== loanId);
                    this.setState({loans:newLoans});
                }
            })
    };

    private updateRemain = (loanId : number,addedOrSubtractedAmount : number) => {
        const editedLoans = this.state.loans.map(l => {
            if(l.id !== loanId){
                return l;
            }
            const loan = l;
            loan.remain = parseInt(l.remain as any,10) + parseInt(addedOrSubtractedAmount as any,10);
            return loan;
        });

        this.setState({loans:editedLoans});
    }

    private getInstallmentList = (loanId : number) => {
        //
    };

    private addInstallment = (installment : Installment) => {
        //
    };

    private editInstallment = (installment : Installment) => {
        //
    };

    private deleteInstallment = (installmentId : number) => {
        //
    };
}

export default LoanListContainer;