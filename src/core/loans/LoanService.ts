import {Loan} from "../../models/Loan";
import DataService from "../common/services/DataService";

class LoanService {
    public static createLoan(loan: Loan): Promise<number> {
        return DataService.PostJson('/loan',loan)
            .then(res => res ? res.loanId : null);
    }

    public static getAllLoans() : Promise<Loan[]>{
        return DataService.GetJson('/loan');
    }

    public static editLoan(loan: Loan) : Promise<any>{
        return DataService.PutJson('/loan',loan.id,loan);
    }

    public static deleteLoan(loanId: number) : Promise<any>{
        return DataService.DeleteJson('/loan',loanId);
    }
}

export default LoanService;