import * as React from 'react';
import {Loan} from "../../models/Loan";
import {Installment} from "../../models/Installment";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import Icon from "@material-ui/core/Icon/Icon";
import Paper from "@material-ui/core/Paper/Paper";
import Button from "@material-ui/core/Button/Button";
import IconButton from "@material-ui/core/IconButton/IconButton";
import {css} from "emotion";
import withTheme, {WithTheme} from "@material-ui/core/styles/withTheme";
import DeleteLoanDialog from "./DeleteLoanDialog";
import InstallmentDialogContainer from "../installments/InstallmentDialogContainer";


interface IPropTypes {
    loans:Loan[];
    editLoan : (loan : Loan) => void;
    deleteLoan : (loanId : number) => void;
    getInstallmentList : (loanId : number) => void;
    addInstallment: (installment : Installment) => void;
    editInstallment: (installment : Installment) => void;
    deleteInstallment: (installmentId : number) => void;
    updateRemain : (loanId : number, addedOrSubtractedAmount : number) => void;
}

interface IStateTypes {
    isEditDialogOpen: boolean;
    isDeleteDialogOpen: boolean;
    isInstallmentDialogOpen: boolean;
    clickedLoan?: Loan;
}

class LoanList extends React.Component<IPropTypes & WithTheme,IStateTypes>{
    public state = {
        isEditDialogOpen: false,
        isDeleteDialogOpen: false,
        isInstallmentDialogOpen: false,
        clickedLoan : undefined
    };

    public render() {
        const {loans} = this.props;
        const clickedLoan = this.state.clickedLoan as any;
        return(
            <div className={style(this.props)}>
                <Paper className={'paper'}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell className={'table-cell'}>شماره وام</TableCell>
                                <TableCell className={'table-cell'}>صاحب وام</TableCell>
                                <TableCell className={'table-cell'}>مبلغ</TableCell>
                                <TableCell className={'table-cell'}>تعداد اقساط</TableCell>
                                <TableCell className={'table-cell'}>مبلغ اولین قسط</TableCell>
                                <TableCell className={'table-cell'}>مبلغ بقیه اقساط</TableCell>
                                <TableCell className={'table-cell'}>کارمزد</TableCell>
                                <TableCell className={'table-cell'}>مانده</TableCell>
                                <TableCell className={'table-cell'}>تاریخ شروع</TableCell>
                                <TableCell className={'table-cell'}>اقساط</TableCell>
                                <TableCell className={'table-cell'}>عملیات</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {loans.map((loan,index) => {
                                return (
                                    <TableRow key={index}>
                                        <TableCell className={'table-cell'}>
                                            {loan.id}
                                        </TableCell>
                                        <TableCell className={'table-cell'}>{loan.username}</TableCell>
                                        <TableCell className={'table-cell'}>{loan.amount}</TableCell>
                                        <TableCell className={'table-cell'}>{loan.monthDuration}</TableCell>
                                        <TableCell className={'table-cell'}>{loan.firstInstallmentAmount}</TableCell>
                                        <TableCell className={'table-cell'}>{loan.installmentAmount}</TableCell>
                                        <TableCell className={'table-cell'}>{(loan.wageFactor * 100) + '%'}</TableCell>
                                        <TableCell className={'table-cell'}>{loan.remain}</TableCell>
                                        <TableCell className={'table-cell'}>{loan.startDate}</TableCell>
                                        <TableCell className={'table-cell'}>
                                            <Button
                                                className={'installment-btn'}
                                                variant={'raised'}
                                                size={"small"}
                                                onClick={this.toggleDialog('isInstallmentDialogOpen',true,loan)}
                                                color={'secondary'}
                                            >
                                                <Icon className={'icon'}>format_list_bulleted</Icon>
                                                اقساط
                                            </Button>
                                        </TableCell>
                                        <TableCell className={'table-cell'}>
                                            <IconButton className={'edit-btn'} onClick={this.toggleDialog('isEditDialogOpen',true,loan)}>
                                                <Icon>edit</Icon>
                                            </IconButton>
                                            <IconButton className={'delete-btn'} onClick={this.toggleDialog('isDeleteDialogOpen',true,loan)}>
                                                <Icon>delete</Icon>
                                            </IconButton>

                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </Paper>
                <DeleteLoanDialog
                    loan={this.state.clickedLoan}
                    isOpen={this.state.isDeleteDialogOpen}
                    onClose={this.toggleDialog("isDeleteDialogOpen",false,undefined)}
                    onSubmit={this.props.deleteLoan}
                />
                <InstallmentDialogContainer
                    isOpen={this.state.isInstallmentDialogOpen}
                    onClose={this.toggleDialog('isInstallmentDialogOpen',false,undefined)}
                    loan={clickedLoan}
                    onRemainUpdate={this.props.updateRemain}
                />
            </div>
        );
    };

    private toggleDialog = (dialogName : string,isOpen: boolean,clickedLoan) => () => {
        this.setState({[dialogName] : isOpen,clickedLoan} as any);
    };
}

const style = (props) => {
    const {theme} = props;
    // language=LESS
    return css`
        &{
            .paper {
                margin-right: 10px;
                margin-left: 10px;
                margin-top: 30px;
            
                .installment-btn {
                    background-color : ${theme.palette.secondary.main};
                    .icon {
                        margin-left:5px;
                    }
                }   
                .edit-btn {
                }
                .delete-btn {
                    color : ${theme.palette.error.main}
                }
                .table-cell {
                  padding: 4px;
                }
            }
        }
    `;
};

export default withTheme()(LoanList);