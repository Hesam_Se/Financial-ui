import {User} from "../../models/User";
import DataService from "../common/services/DataService";

class UserService {
    public static getAllUsers() : Promise<User[]> {
        return DataService.GetJson("/user");
    }

    public static AddUser(user : User) : Promise<any>{
        return DataService.PostJson("/user",user).then(res => res ? res.userId : null);
    }

    public static EditUser(user : User) : Promise<any> {
        return DataService.PutJson("/user",user.id,user);
    }

    public static DeleteUser(userId : number) : Promise<any> {
        return DataService.DeleteJson("/user",userId)
    }
}

export default UserService;