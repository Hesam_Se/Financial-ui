import * as React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import {User} from "../../models/User";
import {css} from "emotion";

interface IState {
    firstName: string;
    lastName: string;
    nationalId: string;
    phoneNumber: string;
    initBalance?: number;
}

interface IProps {
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (user: User, initBalance?: number) => void;
}


class AddUserDialog extends React.Component<IProps, IState> {
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            nationalId: "",
            phoneNumber: "",
            initBalance: undefined,
        }
    }

    public render() {
        return (
            <Dialog classes={{root: styles(this.props)}} open={this.props.isOpen} onClose={this.props.onClose}>
                <DialogTitle>کاربر جدید</DialogTitle>
                <DialogContent>
                    <div className={'row'}>
                        <TextField
                            label={"نام"}
                            value={this.state.firstName}
                            onChange={this.handleChange('firstName')}
                            className={'form-control col'}
                        />
                        <TextField
                            label={"نام خانوادگی"}
                            value={this.state.lastName}
                            onChange={this.handleChange('lastName')}
                            className={'form-control col'}
                        />
                    </div>

                    <div className={'row'}>
                        <TextField
                            label={"کد ملی"}
                            value={this.state.nationalId}
                            onChange={this.handleChange('nationalId')}
                            className={'form-control col'}
                        />

                        <TextField
                            label={"شماره موبایل"}
                            value={this.state.phoneNumber}
                            onChange={this.handleChange('phoneNumber')}
                            className={'form-control col'}
                        />
                    </div>

                    <div className={'row'}>
                        <TextField
                            label={"موجودی اولیه"}
                            value={this.state.initBalance}
                            onChange={this.handleChange('initBalance')}
                            className={'form-control col'}
                        />
                    </div>

                </DialogContent>
                <DialogActions>
                    <Button variant={'raised'} color={'secondary'} onClick={this.onSubmit}>ایجاد</Button>
                    <Button variant={'raised'} onClick={this.props.onClose}>انصراف</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private initState = () => {
        this.setState({
            firstName: "",
            lastName: "",
            nationalId: "",
            phoneNumber: "",
            initBalance: undefined,
        });
    };

    private handleChange = (fieldName: string) => (event) => {
        this.setState({[fieldName]: event.target.value} as any);
    };

    private onSubmit = () => {
        const user = new User(
            0,
            this.state.firstName,
            this.state.lastName,
            this.state.nationalId,
            null,
            this.state.phoneNumber,
            this.state.initBalance,
            ''
        );
        this.props.onSubmit(user);
        this.props.onClose();
        this.initState();
    };
}

const styles = (props) => {
    return css`
        .row {
            display : flex
        }
        .col {
            flex : 50%;
        }
        .form-control {
            margin-left : 20px;
        }
    `
};

export default AddUserDialog;