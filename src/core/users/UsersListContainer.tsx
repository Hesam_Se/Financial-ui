import * as React from 'react';
import {User} from "../../models/User";
import UsersList from "./UsersList";
import {Loan} from "../../models/Loan";
import LoanService from "../loans/LoanService";
import UserService from "./UserService";

interface IState {
    users : User[];
}

class UsersListContainer extends React.Component<{},IState>{
    constructor(props){
        super(props);
        this.state = {
            users : [],
        }
    }

    public componentDidMount(){
        // todo : fetchUser
        UserService.getAllUsers().then(users => {
            this.setState({users});
        });
    }

    public render(){
        return(
            <UsersList
                users={this.state.users}
                addUser={this.addUser}
                editUser={this.editUser}
                deleteUser={this.deleteUser}
                createLoan={this.createLoan}
            />
        );
    }

    private addUser = (user : User) => {
        UserService.AddUser(user).then((userId)=>{
            if(userId) {
                user.id = userId;
                this.setState({users:[...this.state.users,user]});
            }
        });
    };

    private editUser = (user : User) => {
        UserService.EditUser(user).then((isEdited)=>{
            if(isEdited) {
                const {users} = this.state;
                const updatedUsers = users.filter(u => u.id !== user.id);
                updatedUsers.push(user);
                this.setState({users:updatedUsers});
            }
        });
    };

    private deleteUser = (user : User) => {
        UserService.DeleteUser(user.id).then((isDeleted)=>{
            if(isDeleted) {
                const {users} = this.state;
                const updatedUsers = users.filter(u => u.id !== user.id);
                this.setState({users:updatedUsers});
            }
        });
    };

    private createLoan = async (loan : Loan) => {
        const loanId = await LoanService.createLoan(loan);
        const newUsers = this.state.users.map(u=>{
            if(u.id === loan.userId){
                const user = u;
                user.loanId = loanId;
                return user;
            }
            return u;
        });
        this.setState({users:newUsers});
    }
}

export default UsersListContainer;