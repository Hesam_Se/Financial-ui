import * as React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import {User} from "../../models/User";
import {css} from "emotion";

interface IState {
    id: number;
    firstName: string;
    lastName: string;
    nationalId: string;
    phoneNumber: string;
}

interface IProps {
    user? : User;
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (user: User) => void;
}


class EditUserDialog extends React.Component<IProps, IState> {
    constructor(props) {
        super(props);
        const {user} = this.props;
        this.state = {
            id: user ? user.id : 0,
            firstName: user ? user.firstName : "",
            lastName: user ? user.lastName : "",
            nationalId: user ? user.nationalId : "",
            phoneNumber: user ? user.phoneNumber : "",
        }
    }

    public componentWillReceiveProps(newProps) {
        if(newProps.user !== this.props.user){
            const {user} = newProps;
            this.setState({
                id: user ? user.id : "",
                firstName: user ? user.firstName : "",
                lastName: user ? user.lastName : "",
                nationalId: user ? user.nationalId : "",
                phoneNumber: user ? user.phoneNumber : "",
            })
        }
    }

    public render() {
        return (
            <Dialog classes={{root: styles(this.props)}} open={this.props.isOpen} onClose={this.props.onClose}>
                <DialogTitle>کاربر جدید</DialogTitle>
                <DialogContent>
                    <div className={'row'}>
                        <TextField
                            label={"نام"}
                            value={this.state.firstName}
                            onChange={this.handleChange('firstName')}
                            className={'form-control col'}
                        />
                        <TextField
                            label={"نام خانوادگی"}
                            value={this.state.lastName}
                            onChange={this.handleChange('lastName')}
                            className={'form-control col'}
                        />
                    </div>

                    <div className={'row'}>
                        <TextField
                            label={"کد ملی"}
                            value={this.state.nationalId}
                            onChange={this.handleChange('nationalId')}
                            className={'form-control col'}
                        />

                        <TextField
                            label={"شماره موبایل"}
                            value={this.state.phoneNumber}
                            onChange={this.handleChange('phoneNumber')}
                            className={'form-control col'}
                        />
                    </div>

                </DialogContent>
                <DialogActions>
                    <Button variant={'raised'} color={'secondary'} onClick={this.onSubmit}>ویرایش</Button>
                    <Button variant={'raised'} onClick={this.props.onClose}>انصراف</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private handleChange = (fieldName: string) => (event) => {
        this.setState({[fieldName]: event.target.value} as any);
    };

    private onSubmit = () => {
        const user = new User(
            this.state.id,
            this.state.firstName,
            this.state.lastName,
            this.state.nationalId,
            null,
            this.state.phoneNumber,
            0,
            null);
        this.props.onSubmit(user);
        this.props.onClose();
    };
}

const styles = (props) => {
    return css`
        .row {
            display : flex
        }
        .col {
            flex : 50%;
        }
        .form-control {
            margin-left : 20px;
        }
    `
};

export default EditUserDialog;