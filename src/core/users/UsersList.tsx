import * as React from 'react';
import Table from "@material-ui/core/Table/Table";
import Paper from "@material-ui/core/es/Paper/Paper";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import {WithTheme} from "@material-ui/core";
import {css} from "emotion";
import {User} from "../../models/User";
import withTheme from "@material-ui/core/styles/withTheme";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Icon from "@material-ui/core/es/Icon/Icon";
import Button from "@material-ui/core/Button/Button";
import AddUserDialog from "./AddUserDialog";
import EditUserDialog from "./EditUserDialog";
import DeleteUserDialog from "./DeleteUserDialog";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import Popover from "@material-ui/core/Popover/Popover";
import CreateLoanDialog from "../loans/CreateLoanDialog";
import {Loan} from "../../models/Loan";
import BalanceDialogContainer from "../balances/BalanceDialogContainer";


interface IProps extends WithTheme {
    users : User[];
    addUser : (user : User,initBalance : number) => void;
    editUser : (user : User) => void;
    deleteUser : (user : User) => void;
    createLoan : (loan : Loan) => Promise<any>;
}

interface IState {
    isAddDialogOpen : boolean;
    isEditDialogOpen : boolean;
    isDeleteDialogOpen : boolean;
    isCreateLoanDialogOpen : boolean;
    isBalanceDialogOpen : boolean;
    isMenuOpen: boolean;
    clickedUser? : User;
    anchorEl: any;
}

class UsersList extends React.Component<IProps,IState> {
    constructor(props){
        super(props);
        this.state = {
            isAddDialogOpen : false,
            isEditDialogOpen : false,
            isDeleteDialogOpen : false,
            isCreateLoanDialogOpen : false,
            isBalanceDialogOpen : false,
            isMenuOpen : false,
            clickedUser : undefined,
            anchorEl: undefined,
        }
    }

    public render() {
        const {users} = this.props;
        const clickedUser = this.state.clickedUser as User;
        return(
            <div className={style(this.props)}>
                <Paper className={'paper'}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>#</TableCell>
                                <TableCell>نام و نام خانوادگی</TableCell>
                                <TableCell>کد ملی</TableCell>
                                <TableCell>شماره همراه</TableCell>
                                <TableCell>وام دارد؟</TableCell>
                                <TableCell>عملیات</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users.map((user,index) => {
                                return (
                                    <TableRow key={index}>
                                        <TableCell component="th" scope="row">
                                            {index + 1}
                                        </TableCell>
                                        <TableCell>{user.firstName + ' ' + user.lastName}</TableCell>
                                        <TableCell>{user.nationalId}</TableCell>
                                        <TableCell>{user.phoneNumber}</TableCell>
                                        <TableCell>{user.loanId ? <Icon>check</Icon> : <Icon>close</Icon> }</TableCell>
                                        <TableCell>
                                            <IconButton
                                                className={'finance-btn'}
                                                onClick={this.toggleMenu(true,user)}
                                            >
                                                <Icon>attach_money</Icon>
                                            </IconButton>
                                            <IconButton
                                                className={'edit-btn'}
                                                onClick={this.toggleDialog('isEditDialogOpen',true,user)}
                                            >
                                                <Icon>edit</Icon>
                                            </IconButton>
                                            <IconButton
                                                className={'delete-btn'}
                                                onClick={this.toggleDialog('isDeleteDialogOpen',true,user)}
                                            >
                                                <Icon>delete</Icon>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </Paper>
                <Button
                    classes={{root:"add-btn"}}
                    color={"secondary"}
                    variant={"fab"}
                    onClick={this.toggleDialog('isAddDialogOpen',true)}
                >
                    <Icon>add</Icon>
                </Button>
                <AddUserDialog
                    isOpen={this.state.isAddDialogOpen}
                    onClose={this.toggleDialog('isAddDialogOpen',false)}
                    onSubmit={this.props.addUser}
                />
                <EditUserDialog
                    user={this.state.clickedUser}
                    isOpen={this.state.isEditDialogOpen}
                    onClose={this.toggleDialog('isEditDialogOpen',false)}
                    onSubmit={this.props.editUser}
                />
                <DeleteUserDialog
                    user={this.state.clickedUser}
                    isOpen={this.state.isDeleteDialogOpen}
                    onClose={this.toggleDialog('isDeleteDialogOpen',false)}
                    onSubmit={this.props.deleteUser}
                />
                <CreateLoanDialog
                    userId={clickedUser ? clickedUser.id : 0}
                    username={clickedUser ? clickedUser.firstName + ' ' + clickedUser.lastName : ''}
                    isOpen={this.state.isCreateLoanDialogOpen}
                    onClose={this.toggleDialog('isCreateLoanDialogOpen',false)}
                    onSubmit={this.props.createLoan}
                />
                <BalanceDialogContainer
                    isOpen={this.state.isBalanceDialogOpen}
                    onClose={this.toggleDialog('isBalanceDialogOpen',false)}
                    userId={clickedUser ? clickedUser.id : 0}
                    username={clickedUser ? clickedUser.firstName + ' ' + clickedUser.lastName : ''}
                />
                <Popover
                    anchorEl={this.state.anchorEl}
                    open={this.state.isMenuOpen}
                    onClose={this.toggleMenu(false)}
                    anchorOrigin={{
                        vertical:'bottom',
                        horizontal:'center'
                    }}
                    transformOrigin={{
                        vertical:'top',
                        horizontal:'center'
                    }}
                >
                    {(this.state.clickedUser && !this.state.clickedUser.loanId) &&
                        <MenuItem onClick={this.toggleDialog('isCreateLoanDialogOpen',true,this.state.clickedUser)}>ثبت وام</MenuItem>
                    }
                    <MenuItem onClick={this.toggleDialog('isBalanceDialogOpen',true,this.state.clickedUser)}>موجودی ها</MenuItem>
                </Popover>
            </div>
        )
    }

    private toggleDialog = (dialogName,isOpen,clickedUser?: User) => () => {
        this.setState({[dialogName] : isOpen,clickedUser,isMenuOpen: false} as any,()=>{console.log(this.state)});
    };

    private toggleMenu = (isOpen : boolean,user?: User) => (ev) => {
        this.setState({
            isMenuOpen: isOpen,
            anchorEl: ev.currentTarget,
            clickedUser: user,
        })
    }
}

const style = (props) => {
    const {theme} = props;
    return css`
        &{
            .paper {
                margin-right: 50px;
                margin-left: 50px;
                margin-top: 30px;
            
                .finance-btn {
                    color : ${theme.palette.secondary.main}
                }   
                .edit-btn {
                }
                .delete-btn {
                    color : ${theme.palette.error.main}
                }
            }
            .add-btn {
                position: fixed;
                right: 20px;
                bottom: 20px;
                z-index: 1;
                width: 75px;
                height: 75px;
            }
        }
    `;
};

export default withTheme()(UsersList);