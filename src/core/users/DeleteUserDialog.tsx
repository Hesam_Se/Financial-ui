import * as React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import {User} from "../../models/User";
import {css} from "emotion";
import Typography from "@material-ui/core/Typography/Typography";

interface IProps {
    user? : User;
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (user: User) => void;
}


class DeleteUserDialog extends React.Component<IProps,{}> {
    constructor(props) {
        super(props);
    }

    public render() {
        const user = this.props.user as User;
        return (
            <Dialog classes={{root: styles(this.props)}} open={this.props.isOpen} onClose={this.props.onClose}>
                <DialogTitle>حذف کاربر</DialogTitle>
                <DialogContent>
                    {user &&
                        <Typography variant={'body2'}>
                            آیا مایل به حذف {user.firstName + ' ' + user.lastName} هستید؟
                        </Typography>
                    }
                </DialogContent>
                <DialogActions>
                    <Button variant={'raised'} color={'secondary'} onClick={this.onSubmit}>حذف</Button>
                    <Button variant={'raised'} onClick={this.props.onClose}>انصراف</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private onSubmit = () => {
        const user = this.props.user as User;
        this.props.onSubmit(user);
        this.props.onClose();
    };
}

const styles = (props) => {
    return css`
        .row {
            display : flex
        }
        .col {
            flex : 50%;
        }
        .form-control {
            margin-left : 20px;
        }
    `
};

export default DeleteUserDialog;